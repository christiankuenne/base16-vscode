# base16-vscode

This repository is meant to work with [chriskempson/base16](https://github.com/chriskempson/base16).
It provides a simple template that can be used with the base16 color schemes to generate a functional theme for 
[Microsoft/vscode](https://github.com/Microsoft/vscode).

The template is based on [golf1052/base16-vscode](https://github.com/golf1052/base16-vscode).
